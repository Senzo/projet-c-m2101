# Projet C M2101

Projet de M2101 des étudiants Samuel De Amoedo Amorim et Kilyan Le Gallic portant
sur le traitement de trames GPS NMEA format GPGGA en utilisant le langage C

```c
typedef struct {
   char heure[3]; //06
   char minute[3]; //40
   char secondes[3]; //36
   char centiemes[5];//.289
} Temps;

typedef struct { //Nous supposons que l'orientation est Lat,N,Lon,E
   char latitude[11]; //4836.5375
   char longitude[12]; //00740.9373
} Trame;

typedef enum {OK,PAS_GPGGA,CHAMP_INVALIDE,PTR_NULL} Exception;

//Recupere une sous-chaine d'une chaine ch de l'indice debut+orign jusqu'à n
char * retrievesubstr(char *ch,const char *orign,int debut, int n);

//Indique si la chaine ch passée en paramètre est une trame GPGGA
//Retourne 1 si ch est une trame GPGGA
//Lève PAS_GPGGA si ch n'est pas une trame GPGGA
int estGGPA(char ch[],jmp_buf ptRep);

//Convertit une chaine de caractères ch en float p, et transforme p en degré
//minute et secondes
//Retourne un pointeur char
//Lève PTR_NULL si ch==NULL
char * convertirLonLat(char * ch, jmp_buf ptRep);

//Formate une chaine str construite à partir des champs d'une structure
//Temps, au format XXhYYmZZs
//Lève PTR_NULL si tem==NULL
char * formaterHeure(Temps * tem, jmp_buf ptRep);

//Affiche le temps de réception au format XXhYYmZZs
//Lève PTR_NULL si tem==NULL
//Utilise la fonction formaterHeure
void affichageHeure(Temps * tem, jmp_buf ptRep);

//Affiche la latitude et la longitude au format XX°YY'ZZ''
//Lève PTR_NULL si trm==NULL
//Utilise la fonction convertirLonLat
void affichageLonLat(Trame * trm, jmp_buf ptRep);

//Affecte aux structures Temps temps et Trame trame les informations
//voulues en fonction de la position du séparateur ',' dans la chaine trm
//Lève PTR_NULL si temps==NULL ou trame ==NULL
//Utilise la fonction retrievesubstr
void recupererInfo(char * trm,Trame * trame, Temps * temps, jmp_buf ptRep);